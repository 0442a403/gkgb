def degreeToPosition(degree):
    return 1500 + (degree / 90.0) * 800

class Servo:
    def __init__(self, servoPosition):
        self.servoPosition = servoPosition

    def move(self, degree, time):
        return "#" + str(self.servoPosition) + " P" + str(degreeToPosition(degree)) + " T" + str(time)

