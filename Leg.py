from Servo import Servo
from Timing import Timing


class Leg:
    def __init__(self, servo0, servo1, servo2):
        self.servos = [Servo(servo0),
                       Servo(servo1),
                       Servo(servo2)]

    def moveServo(self, servoPosition, degree, time):
        return self.servos[servoPosition].move(degree, time) + " \r\n"

    def takePosition(self, legPosition, time):
        if not time:
            time = [Timing.STANDARD, Timing.STANDARD, Timing.STANDARD]
        res = ""
        for i in range(3):
            res += self.servos[i].move(legPosition.degrees[i], time[i]) + "\r\n"
        return res