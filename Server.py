# coding=utf-8
import socket

import serial

from Controller import Controller
from Degrees import Degrees
from Leg import Leg
from LegPosition import LegPosition
from PermanentAction import PermanentAction
from Position import Position
from Timing import Timing

# Попытка опознать Raspberry Pi3
ssc32 = serial.Serial('/dev/ttyUSB0', 115200, timeout=1.0)

# Инициализация сокета
mSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
PORT = 9002
HOST = "192.168.1.39"
mSocket.bind((HOST, PORT))
mSocket.listen(1)
print("Socket is open for " + HOST + " by " + str(PORT) + " port")

# Инициализация параллельного потока
permanentAction = None

# Инициализация контроллера
controller = Controller(ssc32)

# Инициализация ног
leg0 = Leg(0, 1, 2)
leg1 = Leg(4, 5, 6)
leg2 = Leg(8, 9, 10)
leg3 = Leg(16, 17, 18)
leg4 = Leg(20, 21, 22)
leg5 = Leg(24, 25, 26)

# Инициализация констант
man = Degrees.MARGINAL_NEGATIVE
min = Degrees.MIDDLE_NEGATIVE
ln = Degrees.LIGHT_NEGATIVE
st = Degrees.STANDARD
lp = Degrees.LIGHT_POSITIVE
mip = Degrees.MIDDLE_POSITIVE
map = Degrees.MARGINAL_POSITIVE
tun = Degrees.MIDDLE_NEGATIVE
tup = Degrees.MIDDLE_POSITIVE
standardTime = Timing.STANDARD
acceleratedTime = Timing.ACCELERATED
acceleratedLegTimePack = [acceleratedTime for i in range(3)]
standardLegTimePack = [standardTime for i in range(3)]
acceleratedTimePack = [acceleratedLegTimePack for i in range(6)]
standardTimePack = [standardLegTimePack for i in range(6)]
standardLegPack = [leg0, leg1, leg2, leg3, leg4, leg5]
moveBackLegPack = [leg0, leg1, leg2, leg3, leg4, leg5]
moveLeftBackLegPack = [leg3, leg0, leg1, leg4, leg5, leg2]
moveRightBackLegPack = [leg1, leg2, leg5, leg0, leg3, leg4]
moveLeftForwardLegPack = [leg4, leg3, leg0, leg5, leg2, leg1]
moveRightForwardLegPack = [leg2, leg5, leg4, leg1, leg0, leg3]
moveForwardLegPack = [leg5, leg4, leg3, leg2, leg1, leg0]

# Инициализация позиций ног
standingLegPosition = LegPosition([st, st, st])
sittingLegPosition = LegPosition([st, map, man])

walkingLeg1Position_firstPosition = LegPosition([map, st, st])
walkingLeg1Position_secondPosition = LegPosition([lp, map, man])
walkingLeg1Position_thirdPosition = LegPosition([ln, st, st])

walkingLeg2Position_firstPosition = LegPosition([st, map, man])
walkingLeg2Position_secondPosition = LegPosition([min, st, st])
walkingLeg2Position_thirdPosition = LegPosition([mip, st, st])

walkingLeg3Position_firstPosition = LegPosition([man, st, st])
walkingLeg3Position_secondPosition = LegPosition([lp, st, st])
walkingLeg3Position_thirdPosition = LegPosition([ln, map, man])

walkingLeg4Position_firstPosition = LegPosition([lp, st, st])
walkingLeg4Position_secondPosition = LegPosition([man, st, st])
walkingLeg4Position_thirdPosition = LegPosition([ln, map, man])

walkingLeg5Position_firstPosition = LegPosition([st, map, man])
walkingLeg5Position_secondPosition = LegPosition([mip, st, st])
walkingLeg5Position_thirdPosition = LegPosition([min, st, st])

walkingLeg6Position_firstPosition = LegPosition([ln, st, st])
walkingLeg6Position_secondPosition = LegPosition([lp, map, man])
walkingLeg6Position_thirdPosition = LegPosition([map, st, st])

turningLegPosition_firstPosition = LegPosition([st, st, st])
turningLegPosition_secondPosition = LegPosition([tun, st, st])
turningLegPosition_thirdPosition = LegPosition([st, map, man])
turningLegPosition_fourthPosition = LegPosition([tup, st, st])


# Инициализация позиций ходьбы
standingPosition = Position([standingLegPosition for i in range(6)])
sittingPosition = Position([sittingLegPosition for i in range(6)])

walking_firstPosition = Position([walkingLeg1Position_firstPosition,
                                  walkingLeg2Position_firstPosition,
                                  walkingLeg3Position_firstPosition,
                                  walkingLeg4Position_firstPosition,
                                  walkingLeg5Position_firstPosition,
                                  walkingLeg6Position_firstPosition])

walking_secondPosition = Position([walkingLeg1Position_secondPosition,
                                   walkingLeg2Position_secondPosition,
                                   walkingLeg3Position_secondPosition,
                                   walkingLeg4Position_secondPosition,
                                   walkingLeg5Position_secondPosition,
                                   walkingLeg6Position_secondPosition])

walking_thirdPosition = Position([walkingLeg1Position_thirdPosition,
                                  walkingLeg2Position_thirdPosition,
                                  walkingLeg3Position_thirdPosition,
                                  walkingLeg4Position_thirdPosition,
                                  walkingLeg5Position_thirdPosition,
                                  walkingLeg6Position_thirdPosition])

# Инициализация позиций поворота
turning_firstPosition = Position([turningLegPosition_firstPosition,
                                  turningLegPosition_thirdPosition,
                                  turningLegPosition_firstPosition,
                                  turningLegPosition_thirdPosition,
                                  turningLegPosition_firstPosition,
                                  turningLegPosition_thirdPosition])

turning_secondPosition = Position([turningLegPosition_secondPosition,
                                   turningLegPosition_fourthPosition,
                                   turningLegPosition_secondPosition,
                                   turningLegPosition_fourthPosition,
                                   turningLegPosition_secondPosition,
                                   turningLegPosition_fourthPosition])

turning_thirdPosition = Position([turningLegPosition_thirdPosition,
                                  turningLegPosition_firstPosition,
                                  turningLegPosition_thirdPosition,
                                  turningLegPosition_firstPosition,
                                  turningLegPosition_thirdPosition,
                                  turningLegPosition_firstPosition])

turning_fourthPosition = Position([turningLegPosition_fourthPosition,
                                   turningLegPosition_secondPosition,
                                   turningLegPosition_fourthPosition,
                                   turningLegPosition_secondPosition,
                                   turningLegPosition_fourthPosition,
                                   turningLegPosition_secondPosition])

while True:
    # Подключение клиента
    print("Waiting new connection")
    (connection, addr) = mSocket.accept()
    print('Connection from ' + str(addr))
    while True:

        # Чтение входящих данных и их декодирование
        data = connection.recv(1024)
        msg = data.decode()
        print('Got a message: ' + msg)
        if permanentAction:
            permanentAction.stop()
            permanentAction.join()
            permanentAction = None

        # Выполнение действия в соответствии с входными данными
        if msg == "Disconnection":
            connection.send(b"Disconnection")
            connection.close()
            break

        elif msg == 'SitDown':
            print("Sitting")
            controller.takePosition(sittingPosition, standardLegPack, acceleratedTimePack)

        elif msg == 'StandUp':
            controller.takePosition(standingPosition, standardLegPack, acceleratedTimePack)

        elif msg == 'Party':
            permanentAction = PermanentAction(controller,
                                              [standardLegPack,
                                               standardLegPack],
                                              [sittingPosition,
                                               standingPosition],
                                              acceleratedTime / 1000.0,
                                              [acceleratedTimePack,
                                               acceleratedTimePack])
            permanentAction.start()

        elif msg == "MoveForward":
            permanentAction = PermanentAction(controller,
                                              [moveForwardLegPack,
                                               moveForwardLegPack,
                                               moveForwardLegPack],
                                              [walking_firstPosition,
                                               walking_secondPosition,
                                               walking_thirdPosition],
                                              standardTime / 1000.0,
                                              [standardTimePack,
                                               standardTimePack,
                                               standardTimePack])
            permanentAction.start()

        elif msg == "MoveRightForward":
            permanentAction = PermanentAction(controller,
                                              [moveRightForwardLegPack,
                                               moveRightForwardLegPack,
                                               moveRightForwardLegPack],
                                              [walking_firstPosition,
                                               walking_secondPosition,
                                               walking_thirdPosition],
                                              standardTime / 1000.0,
                                              [standardTimePack,
                                               standardTimePack,
                                               standardTimePack])
            permanentAction.start()
            
        elif msg == "MoveRightBack":
            permanentAction = PermanentAction(controller,
                                              [moveRightBackLegPack,
                                               moveRightBackLegPack,
                                               moveRightBackLegPack],
                                              [walking_firstPosition,
                                               walking_secondPosition,
                                               walking_thirdPosition],
                                              standardTime / 1000.0,
                                              [standardTimePack,
                                               standardTimePack,
                                               standardTimePack])
            permanentAction.start()

        elif msg == "MoveBack":
            permanentAction = PermanentAction(controller,
                                              [moveBackLegPack,
                                               moveBackLegPack,
                                               moveBackLegPack],
                                              [walking_firstPosition,
                                               walking_secondPosition,
                                               walking_thirdPosition],
                                              standardTime / 1000.0,
                                              [standardTimePack,
                                               standardTimePack,
                                               standardTimePack])
            permanentAction.start()

        elif msg == "MoveLeftBack":
            permanentAction = PermanentAction(controller,
                                              [moveLeftBackLegPack,
                                               moveLeftBackLegPack,
                                               moveLeftBackLegPack],
                                              [walking_firstPosition,
                                               walking_secondPosition,
                                               walking_thirdPosition],
                                              standardTime / 1000.0,
                                              [standardTimePack,
                                               standardTimePack,
                                               standardTimePack])
            permanentAction.start()

        elif msg == "MoveLeftForward":
            permanentAction = PermanentAction(controller,
                                              [moveLeftForwardLegPack,
                                               moveLeftForwardLegPack,
                                               moveLeftForwardLegPack],
                                              [walking_firstPosition,
                                               walking_secondPosition,
                                               walking_thirdPosition],
                                              standardTime / 1000.0,
                                              [standardTimePack,
                                               standardTimePack,
                                               standardTimePack])
            permanentAction.start()

        elif msg == "TurnCounterclockwise":
            permanentAction = PermanentAction(controller,
                                              [standardLegPack,
                                               standardLegPack,
                                               standardLegPack,
                                               standardLegPack],
                                              [turning_firstPosition,
                                               turning_fourthPosition,
                                               turning_thirdPosition,
                                               turning_secondPosition],
                                              standardTime / 1000.0,
                                              [standardTimePack,
                                               standardTimePack,
                                               standardTimePack,
                                               standardTimePack])
            permanentAction.start()

        elif msg == "TurnClockwise":
            permanentAction = PermanentAction(controller,
                                              [standardLegPack,
                                               standardLegPack,
                                               standardLegPack,
                                               standardLegPack],
                                              [turning_firstPosition,
                                               turning_secondPosition,
                                               turning_thirdPosition,
                                               turning_fourthPosition],
                                              standardTime / 1000.0,
                                              [standardTimePack,
                                               standardTimePack,
                                               standardTimePack,
                                               standardTimePack])
            permanentAction.start()

        # Тестовые команды
        elif msg == "Test":
            permanentAction = PermanentAction(controller,
                                              standardLegPack,
                                              [walking_firstPosition,
                                               walking_secondPosition,
                                               walking_thirdPosition],
                                              standardTime / 1000.0,
                                              [standardTimePack,
                                               standardTimePack,
                                               standardTimePack])
            permanentAction.start()

        elif msg == "Walk1":
            controller.takePosition(walking_firstPosition, [leg0, leg1, leg2, leg3, leg4, leg5], standardTimePack)

        elif msg == "Walk2":
            controller.takePosition(walking_secondPosition, [leg0, leg1, leg2, leg3, leg4, leg5], standardTimePack)

        elif msg == "Walk3":
            controller.takePosition(walking_thirdPosition, [leg0, leg1, leg2, leg3, leg4, leg5], standardTimePack)

        else:
            connection.send(b"Disconnection")
            connection.close()
            break

        #connection.send('CompletingTask')
        connection.sendall(b'CompletingTask')
