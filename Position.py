class Position:
    def __init__(self, positions):
        self.positions = positions

    def takePosition(self, legs, time):
        res = ""
        for i in range(len(self.positions)):
        	res += legs[i].takePosition(self.positions[i], time[i])
        return res