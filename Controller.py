# Контроллер роботом
class Controller:
    def __init__(self, ssc32):
        self.ssc32 = ssc32

    # Передать команду SCC32
    def writeSSC32(self, command):
        self.ssc32.write(bytes(command, encoding='utf-8'))

    # Принять позицию
    def takePosition(self, position, legs, time):
        self.writeSSC32(position.takePosition(legs, time))