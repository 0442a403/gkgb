import threading
import time


class PermanentAction(threading.Thread):
    def __init__(self, controller, legs, positions, delay, timing, startPosition=0):
        super().__init__()
        self.isRunning = False
        self.positions = positions
        self.delay = delay
        self.timing = timing
        self.startPosition = startPosition
        self.positionLen = len(positions)
        self.controller = controller
        self.legs = legs

    def start(self):
        self.isRunning = True
        super().start()

    def run(self):
        position = self.startPosition
        while self.isRunning:
            position %= self.positionLen
            self.controller.takePosition(self.positions[position], self.legs[position], self.timing[position])
            position += 1
            time.sleep(self.delay)


    def stop(self):
        self.isRunning = False